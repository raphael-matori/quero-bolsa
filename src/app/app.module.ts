import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { FooterNoteBarComponent } from './footer/footer-note-bar/footer-note-bar.component';
import { FooterUtilLinksComponent } from './footer/footer-util-links/footer-util-links.component';
import { FooterContactUsComponent } from './footer/footer-contact-us/footer-contact-us.component';
import { MainSubscriptionComponent } from './main/main-subscription/main-subscription.component';
import { MainShareWithYourFriendsComponent } from './main/main-share-with-your-friends/main-share-with-your-friends.component';
import { HeaderMenuComponent } from './header/header-menu/header-menu.component';
import { HeaderMainComponent } from './header/header-main/header-main.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    FooterNoteBarComponent,
    FooterUtilLinksComponent,
    FooterContactUsComponent,
    MainSubscriptionComponent,
    MainShareWithYourFriendsComponent,
    HeaderMenuComponent,
    HeaderMainComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

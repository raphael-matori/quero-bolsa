import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-share-with-your-friends',
  templateUrl: './main-share-with-your-friends.component.html',
  styleUrls: ['./main-share-with-your-friends.component.scss']
})
export class MainShareWithYourFriendsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    const copyTextareaBtn = document.querySelector('#btn-copy-ref-link');
    copyTextareaBtn.addEventListener('click', function (event) {
      const copyTextarea: HTMLInputElement = document.querySelector('#my-ref-link');
      copyTextarea.select();
      try {
        const successful = document.execCommand('copy');
      } catch (err) {
      }
    });
  }
}

# Quero Bolsa

Esse projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 6.0.8.

## Rodando a aplicação

Clone o repositório. Execute pelo terminal na pasta da aplicação `npm install` para baixar as dependências.

Após instalado as dependências execute no terminal `ng serve` para subir o servidor da aplicação.

Em seu navegador, acesse `http://localhost:4200/`. 

## Build

Execute `ng build` para buildar o projeto. Os artefatos buildados estarão disponíveis no diretório `dist/`. Use a flag `--prod` para buildar a versão de produção.

## Ajuda

Caso encontre qualquer problema para executar a aplicação, entre em contato comigo por e-mail: raphaelmatori@hotmail.com
